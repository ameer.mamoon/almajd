import 'package:almajd/Language.dart';
import 'package:almajd/Screens/Elements/Drawers.dart';
import 'package:almajd/Screens/Elements/Posts.dart';
import 'package:almajd/Screens/MainPage.dart';
import 'package:almajd/Themes.dart';
import 'package:almajd/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'inputs.dart';

AppLanguage Lang = AppLanguage();
AppTheme _appTheme = AppTheme();

class NavScreen extends StatelessWidget {

  final Widget child;
  final bool Nlogin;
  NavScreen(this.child,{this.Nlogin = true,this.selector});
  int selector;

  @override
  Widget build(BuildContext context) {

    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    Color _SvgColor = _appTheme.theme.iconTheme.color;
    List<Widget> nav = [
      GestureDetector(
        child: SvgPicture.asset(
          'images/Routing${Lang.isAr?'Ar':''}.svg',
          height: h * .08,
          color: _SvgColor,
        ),
        onTap: (){
          Navigator.popAndPushNamed(context, 'Tracking');
        },
      ),
      GestureDetector(
        child: SvgPicture.asset(
          'images/About${Lang.isAr?'Ar':''}.svg',
          height: h * .08,
          color: _SvgColor,
        ),
        onTap: () {
          Navigator.popAndPushNamed(context, 'AboutUs');
        },
      ),
      GestureDetector(
        child: SvgPicture.asset(
          'images/Contact${Lang.isAr?'Ar':''}.svg',
          height: h * .08,
          color: _SvgColor,
        ),
        onTap: () {
          Navigator.popAndPushNamed(context, 'ContactUs');
        },
      ),
      GestureDetector(
        child: SvgPicture.asset(
          'images/Settings${Lang.isAr?'Ar':''}.svg',
          height: h * .08,
          color: _SvgColor,
        ),
        onTap: () {
          Navigator.pushNamed(context, 'Settings');
        },
      ),
    ];
    if(Lang.isAr)
    {
      List<Widget> t = List(4);
      for(int i = 0 ; i < nav.length ; i++)
        t[i] = nav[nav.length - i -1];
      nav = t;
      selector = 3 - selector;
    }


    print('Nav $Nlogin');
    return MainPage(
      child:child,
      NavBar: Container(
        padding: EdgeInsets.symmetric( horizontal: w * .005),
        width: double.infinity,
        height: h * .065,
        color: _appTheme.theme.scaffoldBackgroundColor,
        child: Stack(
          children: [
            Row(children: [
              //Expanded(child: Container(color: Colors.transparent,)),
              Expanded(child: Container(color: (selector == 0)? Colors.grey.withOpacity(0.4): Colors.transparent),flex: 4,),
              Expanded(child: Container(color: (selector == 1)? Colors.grey.withOpacity(0.4): Colors.transparent),flex: 4),
              Expanded(child: Container(color: (selector == 2)? Colors.grey.withOpacity(0.4): Colors.transparent),flex: 4),
              Expanded(child: Container(color: (selector == 3)? Colors.grey.withOpacity(0.4): Colors.transparent,),flex: 4),
              //Expanded(child: Container(color: Colors.transparent,))
            ],),
            Row(children: nav,mainAxisAlignment: MainAxisAlignment.spaceEvenly,),

          ],
        ),
      ),
      drawer: Drawer(
        child: MainDrawer(selector),

      ),
      Nlogin: Nlogin,
    );
  }


}



class Tracking extends StatefulWidget {
  @override
  _TrackingState createState() => _TrackingState();
}
Widget popUp = null;
class _TrackingState extends State<Tracking> {
  @override
  Widget build(BuildContext context) {
    double h= MediaQuery.of(context).size.height;
    List<Widget> _PageContent = [Column(
      children: [
        Container(
          height: h*.06,
          width: double.infinity,

          child: Center(
            child: Text(Lang.get('Tracking'),style: TextStyle(
              fontSize: h*.028,
              color: _appTheme.theme.accentColor,
              fontWeight: FontWeight.bold
            ),),
          ),
        ),
        Expanded(
          child: OrderList(this),
        ),
      ],
    )];
    if(popUp != null)
      _PageContent.add(popUp);

    return NavScreen(
     isLogin? Stack(
      children: _PageContent,
    ):Login(),
    Nlogin: isLogin,selector: 0,
    );
  }
}


class OurService extends StatelessWidget {
  AppTheme _appTheme =AppTheme();
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    TextStyle title = TextStyle(
        color: _appTheme.theme.accentColor,
        fontSize: h*.03,
        fontWeight: FontWeight.bold
    );

    TextStyle txt = TextStyle(
        color: _appTheme.theme.accentColor,
        fontSize: h*.022
    );


    return Container(
      padding: EdgeInsets.all(w*.025),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(w * .025),
              child: Text(
                Lang.get('OurServices'),
                style: TextStyle(
                  fontSize: h * .028,
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            // _TextWithTitle('${Lang.get('Shipping')} :','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,',title,txt),
            // _TextWithTitle('${Lang.get('AirTransport')} :','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,',title,txt),
            // _TextWithTitle('${Lang.get('RoadTransport')} :','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,',title,txt),

            Lang.isAr?
            Text("""
نضع بين ايديكم شبكة عالمية ومحلية من وكلاء الشحن المحترفين تغطي كل بقاع العالم, تعمل بتناغم مطلق لتصل بضائعكم الى مقصدها بسلامة وضمن شروطكم مهما كانت الظروف.

 السرعة,الأمان,الخدمة والاسعار المنافسة ... هي رمز العمل المتطور المتكامل وهي ما تقدمه لكم شركة المجد في سوريا و حول العالم.

خدماتنا :

    -خدمات لوجستية

    -شحن بحري و جوي من جميع انحلء العالم الى سورية

    -شحن و تخليص حوايا كاملة

    -شحن جزئي من جميع انحاء الصين مهما كانت الكمية

    -نقل داخلي من جميع انحاء الصين الى مستودعاتنا في الصين بافضل الطرق و ارخص الاسعار.

    -تخليص البضائع في جميع المنافذ البحرية والبرية السورية.

    -الشحن من الباب إلى الباب.

    -كادر متخصص في التغليف والتحميل بأحدث الوسائل المتوفرة

    - مستودعاتنا جاهزة عل مدار الساعة لاستقبال بضائعكم
            """,textDirection: TextDirection.rtl,textScaleFactor: 0.99,)


                :Text("""
We put in your hands a global and local network of professional shipping agents covering all parts of the world, working in absolute harmony to reach your goods to their destination safely and within your conditions, whatever the circumstances.

Speed, safety, service and competitive prices ... it is the symbol of an advanced, integrated work that is what Al-Majd Company offers you in Syria and around the world.
     
     - Logistical services
     
     - Sea and air freight from all over the world to Syria
     
     - Full container shipping and clearance
     
     - Partial shipment from all over China, whatever the quantity
     
     - Internal transportation from all parts of China to our warehouse in China with the best and cheapest way
     
     - Clearance of goods at all Syrian sea and land ports
     
     - Door-to-door shipping
     
     - Staff specialized in packing and loading with the latest available means
     
     - Our warehouse is ready around the clock to receive your goods
            """,textScaleFactor: 0.99,)

          ],
        ),
      ),
    );
  }

  Widget _TextWithTitle(String title,String text,TextStyle titleStyle,TextStyle textStyle){
    return Container(
        margin: EdgeInsets.only(bottom: titleStyle.fontSize),
        child: Column(

          children: [
            Row(
              mainAxisAlignment: Lang.isAr?MainAxisAlignment.end:MainAxisAlignment.start,
              children: [
                Text(title,style: titleStyle,textAlign:Lang.Align(),textDirection:Lang.Direction(),),
              ],
            ),
            Text(text,style: textStyle,),
          ],
        )
    );
  }
}