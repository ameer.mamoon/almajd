import 'package:almajd/Screens/Elements/Posts.dart';
import 'package:almajd/Themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:almajd/main.dart';

import 'MainPage.dart';
import 'NavBarScreen.dart';
import 'inputs.dart';

class Welcome extends StatelessWidget {


  Future<Widget> _PushApp() async{
    await Future.delayed(Duration(seconds:2));
    return (await SavedData.get()).getValue('firstOpen')?MainPage(child: Language(),):Tracking();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    return FutureBuilder(
        future: _PushApp(),
        builder: (BuildContext context,AsyncSnapshot<Widget> snapshot ){
      if(snapshot.hasData)
        return snapshot.data;
      return  Container(
        width: double.infinity,
        height: double.infinity,
        color: AppTheme().isLight() ? Color(0xFFF2F2F0) : Color(0xFF171717),
        child: Center(

          child: Padding(
            padding:  EdgeInsets.all(h*0.1),
            child: AppTheme().isLight() ? Image.asset('images/logo.png',):Image.asset('images/DarkLogo.png',),
          ),
        ),
      );
    }
    );
  }
}
