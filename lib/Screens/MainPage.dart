import 'package:almajd/Language.dart';
import 'package:almajd/Themes.dart';
import 'package:almajd/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MainPage extends StatelessWidget {
  final State MyApp;
  final Widget child;
  final Drawer drawer;
  final AppTheme _appTheme = AppTheme();
  final Widget NavBar;
  bool _isLogin = isLogin;
  final bool Nlogin ;

  MainPage({this.MyApp,this.child,this.drawer,this.NavBar = null,this.Nlogin = true});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    List<Widget> AppBarRow = [];

    if(_isLogin && !AppLanguage().isAr)
      AppBarRow.add(
          Expanded(
            child: Container(
              margin: EdgeInsets.all(10),
              child: Container(

                  child: Center(
                    child: FittedBox(
                      child: Text(Name,style: TextStyle(
                        color: _appTheme.theme.accentColor,
                        // fontSize: h*.02
                      ),
                      ),
                    ),
                  )
              ),
            ),
          )
      );


    if(_isLogin && AppLanguage().isAr)
      AppBarRow.add(
          Expanded(
            child: Container(
              margin: EdgeInsets.all(10),
              child: Container(

                  child: Center(
                    child: FittedBox(
                      child: Text(Name,style: TextStyle(
                        color: _appTheme.theme.accentColor,
                        // fontSize: h*.02
                      ),
                      ),
                    ),
                  )
              ),
            ),
          )
      );
    return Scaffold(
      appBar: _isLogin ?
      AppBar(
        centerTitle: true,
        backgroundColor: _appTheme.theme.appBarTheme.backgroundColor,
        iconTheme: _appTheme.theme.iconTheme,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: AppBarRow,
        ),
        leading: AppLanguage().isAr ? (Nlogin?Image.asset('images/DarkLogo.png',):null):
        Builder(builder: (context){return IconButton(
          icon: Icon(Icons.menu_rounded),
          onPressed: () => Scaffold.of(context).openDrawer(),
        );}),
        actions: AppLanguage().isAr ? [
          Builder(builder: (context){
            return IconButton(
                icon: Icon(Icons.menu),
                onPressed: (){
                  Scaffold.of(context).openEndDrawer();
                }
            );
          })
        ]:(Nlogin?[Image.asset('images/DarkLogo.png',)]:null),

      )
          :AppBar(
        backgroundColor: _appTheme.theme.appBarTheme.backgroundColor,
        iconTheme: _appTheme.theme.iconTheme,
          title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: AppBarRow,
        ),
        leading:drawer != null&& AppLanguage().isAr ?(Nlogin?Image.asset('images/DarkLogo.png',):null):
        Builder(builder: (context){return IconButton(
          icon: Icon(Icons.menu_rounded),
          onPressed: () => Scaffold.of(context).openDrawer(),
        );}),
        automaticallyImplyLeading: false,
        centerTitle: true,
        actions:drawer != null&&AppLanguage().isAr ? [
          Builder(builder: (context){
            return IconButton(
                icon: Icon(Icons.menu),
                onPressed: (){
                  Scaffold.of(context).openEndDrawer();
                }
            );
          }),
        ]:(Nlogin?[Image.asset('images/DarkLogo.png',)]:null),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: _appTheme.boxDecoration,
        child: child,
      ),

      drawer:drawer != null&&AppLanguage().isAr? null:drawer,
      endDrawer:drawer != null&& !AppLanguage().isAr? null:drawer,
      bottomNavigationBar: NavBar,


    );
  }
}
