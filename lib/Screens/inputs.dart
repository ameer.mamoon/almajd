import 'package:almajd/Screens/Elements/Elements.dart';
import 'package:almajd/Themes.dart';
import 'package:almajd/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:almajd/Language.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import 'NavBarScreen.dart';

AppLanguage Lang = AppLanguage();

class Settings extends StatefulWidget {
  State _MyApp;
  bool _LangSwitchValue = Lang.isAr;

  @override
  _SettingsState createState() => _SettingsState();

  Settings(this._MyApp);
}

class _SettingsState extends State<Settings> {
  AppTheme _appTheme = AppTheme();

  bool _ThemeSwitchValue;

  double _fontSize;

  _SettingsState();

  @override
  void initState() {
    _ThemeSwitchValue = !_appTheme.isLight();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double iconSize = h * .045;
    _fontSize = h * .03;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: _appTheme.boxDecoration,
          child: SingleChildScrollView(
            child: Stack(
              children: [
                IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Column(
                  children: [
                    Container(
                        width: w*0.5,
                        padding: EdgeInsets.all(w*0.05),
                        child: Image.asset('images/DarkLogo.png')
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        '${Lang.get('Settings')}',
                        style: TextStyle(
                            fontSize: h * .035,
                            fontWeight: FontWeight.w600,

                            ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: h*0.03),
                      child: SettingsElement(
                          icon: Icon(
                            Icons.translate,
                            size: iconSize,
                          ),
                          text: '${Lang.get('Language')}:',
                          elements: [
                            Text(
                              'En',
                              style: TextStyle(fontSize: _fontSize * .6),
                            ),

                            CupertinoSwitch(
                                  value: widget._LangSwitchValue,
                                  trackColor: Colors.blue,
                                  onChanged: (value) async {
                                    await Lang.SwitchLanguage();
                                    widget._MyApp.setState(() {
                                      widget._LangSwitchValue = Lang.isAr;
                                    });
                                  },
                                  activeColor: Colors.blue,
        ),

                            Text(
                              'ع',
                              style: TextStyle(fontSize: _fontSize * .6),
                            ),
                          ]),
                    ),
                    SettingsElement(
                      icon: _appTheme.ThemeIcon(iconSize),
                      text: '${Lang.get('Theme')}:',
                      elements: [
                        CupertinoSwitch(
                          value: _ThemeSwitchValue,
                          trackColor: Colors.blue,
                          onChanged: (value) {
                            _ThemeSwitchValue = value;
                            widget._MyApp.setState(() {
                              _appTheme.SwitchTheme();
                              print(
                                  '${_appTheme.isLight() ? 'Light' : 'Dark'} Theme');
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                      ],
                    ),
                    isLogin
                        ? GestureDetector(
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: h*0.03),
                              child: SettingsElement(
                                icon: Icon(
                                  Icons.lock,
                                  size: iconSize,
                                ),
                                text: '${Lang.get('ChangePassword')}',
                              ),
                            ),
                            onTap: () {
                              Navigator.pushNamed(context, 'ChangePass');
                            },
                          )
                        : Container(
                            height: h * .1,
                          ),
                    MyButton(
                      text: '${Lang.get('Confirm')}',
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushReplacementNamed(context, 'Tracking');
                        popUp = null;
                        print('Settings-Confirm');
                      },
                      heightFactor: 0.8,
                      fontFactor: Lang.isAr ? 0.8 : 1,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget SettingsElement({Icon icon, String text, List<Widget> elements}) {
    return IconText(
      icon: icon,
      text: text,
      style: TextStyle(
        fontSize: _fontSize,
      ),
      element: elements == null
          ? null
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: elements,
            ),
    );
  }
}

// User --> login & change password
class _UserScreen extends StatelessWidget {
  Widget _content;
  String _text;
  final bool img;

  _UserScreen(this._content, this._text, {this.img = true});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    List<Widget> ListContent = [];
    if (img)
      ListContent.add(Container(
          width: w*0.5,
          padding: EdgeInsets.all(w*0.05),
          child: Image.asset('images/DarkLogo.png')
      ),);

    ListContent.add(Container(
      height: h * .055,
      margin: EdgeInsets.only(bottom: h * 0.025),
      child: FittedBox(
        fit: BoxFit.fitHeight,
        child: Text(
          _text,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).accentColor),
        ),
      ),
    ));
    ListContent.add(Container(
      child: _content,
    ));
    return SingleChildScrollView(
      child: Column(
        children: ListContent,
      ),
    );
  }
}

class Login extends StatefulWidget {
  String _userName, _password;

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  static final TextEditingController userNameCont = TextEditingController();
  static final TextEditingController passNameCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print("""
    Widget :
    user name : ${widget._userName}
    password  : ${widget._password}
    """);

    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return _UserScreen(
        Container(
          child: Container(
            height: h * .5,
            child: Column(
              children: [
                MyTextField(
                  hintText: Lang.get('UserName'),
                  onChanged: (UserName) async {
                    widget._userName = UserName;
                  },
                  controller: userNameCont,
                ),
                MyTextField(
                  hintText: Lang.get('Password'),
                  isPassword: true,
                  // onChanged: (Pass) {
                  //   widget._password = Pass;
                  // },
                  controller: passNameCont,
                ),
                Row(
                  mainAxisAlignment: !Lang.isAr
                      ? MainAxisAlignment.start
                      : MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: w * .045),
                        child: Text(
                          '${Lang.get('ForgotPassword')}',
                          style: TextStyle(fontSize: h * 0.017),
                          textDirection: Lang.Direction(),
                        ),
                      ),
                      onTap: () async {
                        print('Forgot Password ?');
                        await launch(
                          'https://www.almajdscc.com/tracking/password-reset/',
                          forceWebView: true,
                        );
                      },
                    )
                  ],
                ),
                Expanded(child: Container()),
                // MyButton(
                //   text: Lang.get('Login'),
                //   onTap: () async {
                //     await _login(context);
                //     isLogin = token != null;
                //     print('is login $isLogin');
                //     if (isLogin)
                //       Navigator.pushReplacementNamed(context, 'Tracking');
                //   },
                // ),
                ElevatedButton(
                  onPressed: () async {
                    await _login(context);
                    isLogin = token != null;
                    print('is login $isLogin');
                    if (isLogin)
                      Navigator.pushReplacementNamed(context, 'Tracking');
                  },
                  child: Text(Lang.get('Login'),style: TextStyle(color: Theme.of(context).scaffoldBackgroundColor),),

                ),

                Expanded(flex: 4, child: Container()),
              ],
            ),
          ),
        ),
        Lang.get('Login'));
  }

  Future<void> _login(BuildContext context) async {
    print("""
    Login :
    user name : ${userNameCont.text}
    password  : ${passNameCont.text}
    """);

    var request = http.MultipartRequest(
        'POST', Uri.parse('https://almajd.almajdscc.com/api/auth/login'));
    request.fields.addAll({
      'user_login': '${userNameCont.text}',
      'user_pass': '${passNameCont.text}'
    });

    http.StreamedResponse response = await request.send();
    String Json = await response.stream.bytesToString();
    Map<String, dynamic> info = json.decode(Json);

    if (info["error"] != null) {
      print('wrong pass : ${info["error"]} ');
      Toast.show("Wrong Password", context, gravity: Toast.BOTTOM);
    } else {
      print('right pass\n $info');

      var pref = (await SavedData.get());
      Name = info["data"]["display_name"];
      token = info["access_token"];

      password = passNameCont.text;
      print('$Name,$token,$password');

      await pref.saveString('Name', Name);
      await pref.saveString('token', token);
      await pref.saveString('Password', password);
      await pref.saveBool('isLogin', true);

      print('right pass\n $info');
    }
  }
}

class ChangePass extends StatelessWidget {
  TextEditingController _OldPassword;
  TextEditingController _NewPassword;
  TextEditingController _ConfirmNewPassword;

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    return _UserScreen(
        Container(
          child: SingleChildScrollView(
            child: Container(
              height: h * .605,
              child: Column(
                children: [
                  MyTextField(
                    hintText: Lang.get('OldPassword'),
                    isPassword: true,
                    controller: _OldPassword,
                  ),
                  MyTextField(
                    hintText: Lang.get('NewPassword'),
                    isPassword: true,
                    controller: _NewPassword,
                  ),
                  MyTextField(
                    hintText:
                        '${Lang.get('Confirm')} ${Lang.get('NewPassword')}',
                    isPassword: true,
                    controller: _ConfirmNewPassword,
                  ),
                  Expanded(child: Container()),
                  MyButton(
                    text: Lang.get('Confirm'),
                    onTap: () async {
                      if (_OldPassword == password &&
                          _ConfirmNewPassword == _NewPassword) {
                        var headers = {'Authorization': 'Bearer $token'};
                        var request = http.MultipartRequest(
                            'POST',
                            Uri.parse(
                                'https://almajd.almajdscc.com/api/auth/change'));
                        request.fields.addAll({
                          'old_pass': _OldPassword.text,
                          'new_pass': _NewPassword.text,
                          'new_pass_confirmation': _ConfirmNewPassword.text
                        });

                        request.headers.addAll(headers);

                        http.StreamedResponse response = await request.send();

                        if (response.statusCode == 200) {
                          print(await response.stream.bytesToString());
                        } else {
                          print(response.reasonPhrase);
                        }
                        password = _NewPassword.text;
                        (await SavedData.get())
                            .saveString("Password", _NewPassword.text);
                      }

                      Navigator.pop(context);
                    },
                  ),
                  Expanded(flex: 4, child: Container()),
                ],
              ),
            ),
          ),
        ),
        Lang.get('ChangePassword'));
  }
}

class ContactUs extends StatelessWidget {
  TextEditingController _Name = TextEditingController(),
      _Phone = TextEditingController(),
      _Email = TextEditingController(),
      _Desc = TextEditingController();

  String _Shipment;

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return _UserScreen(
      Container(
        child: SingleChildScrollView(
          child: Container(
            height: h * .93,
            child: Column(
              children: [
                MyTextField(
                  hintText: Lang.get('Name') + _getReq(),
                  controller: _Name,
                ),
                MyTextField(
                  hintText: Lang.get('PhoneNumber') + _getReq(),
                  controller: _Phone,
                ),
                MyTextField(
                  hintText: Lang.get('Email') ,
                  controller: _Email,
                ),
                MyDropList(
                  hintText: Lang.get('Confirm'),
                  onChanged: (value) {
                    _Shipment = value;
                  },
                ),
                MyTextArea(
                  titleText: '${Lang.get('DescriptionAboutTheOrder')} ' +_getReq(),
                  controller: _Desc,
                ),
                MyButton(
                  text: Lang.get('Send'),
                  onTap: () {
                    _sendMail(context);
                  },
                  sizeFactor: 0.75,
                  heightFactor: 1.15,
                  fontFactor: 1.1,

                ),
              ],
            ),
          ),
        ),
      ),
      Lang.get('ContactUs'),
      img: true,
    );
  }

  String _getReq() {
    return Lang.isAr ? ' (مطلوب)' : ' (Required)';
  }

  Future<void> _sendMail(BuildContext context) async {
    print("""
    Name : ${_Name.text}
    Phone Number : ${_Phone.text}
    Email : ${_Email.text}
    Shipment Type :  $_Shipment
    The Letter : ${_Desc.text ?? ''}
    """);
    if (_Email.text == null ||
        _Name.text == null ||
        _Phone.text == null ||
        _Email.text.trim() == '' ||
        _Name.text.trim() == '' ||
        _Phone.text.trim() == '') {
      Toast.show("Required Fields Must Be Filled", context,
          gravity: Toast.BOTTOM);
      return;
    }

    await _openEmail(
        toEmail: 'info@almajdscc.com', subject: "Shipment", body: """
    Name : ${_Name.text}
    Phone Number : ${_Phone.text}
    Email : ${_Email.text}
    Shipment Type :  $_Shipment
    The Letter : ${_Desc.text ?? ''}
    """);
  }

  Future _openEmail({
    @required String toEmail,
    @required String subject,
    @required String body,
  }) async {
    final url = 'mailto:$toEmail?subject=${subject}&body=${body}';

    if (await canLaunch(url)) await launch(url);
  }
}

class Language extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset('images/logo.png'),
          Padding(
            padding: EdgeInsets.fromLTRB(w * .1, h * .08, w * .1, h * .05),
            child: textElement(h * .03, Theme.of(context).accentColor,
                '${Lang.get('ChooseYourLanguage')} :'),
          ),
          AnimatedLanguage(
            onChanged: (value) {
              Lang.SwitchLanguage();
              App.setState(() {});
            },
          ),
          Padding(
            padding: EdgeInsets.only(top: h * .07, bottom: h * .048),
            child: MyButton(
              text: Lang.get('Enter'),
              fontFactor: .75,
              onTap: () {
                Navigator.popAndPushNamed(context, 'Tracking');
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: w * .1),
            child: textElement(h * .022, Theme.of(context).accentColor,
                '${Lang.get("you can change this later in the")} ${Lang.get('Settings')}'),
          ),
        ],
      ),
    );
  }

  Widget textElement(double fontSize, Color color, String text) {
    return Row(
      mainAxisAlignment:
          Lang.isAr ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Text(
          text,
          style: TextStyle(
            color: color,
            fontSize: fontSize,
          ),
          textDirection: Lang.isAr ? TextDirection.rtl : TextDirection.ltr,
        ),
      ],
    );
  }
}
