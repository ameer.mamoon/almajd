import 'package:almajd/Language.dart';
import 'package:almajd/Themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

AppLanguage Lang = AppLanguage();


class IconText extends StatelessWidget{

  final Icon icon;
  final String text;
  final TextStyle style;
  final Widget element;


  IconText({this.icon, this.text, this.style,this.element});

  @override
  Widget build(BuildContext context) {
    List<Widget> elementsEn = [
      Expanded(
          flex: 18,
          child: icon
      ),
      Expanded(
          flex: element == null ? 81 : 41,
          child: Text(text,style: style,)
      ),
      Expanded(
          flex: element == null ? 1 : 41,
          child: element == null ? Container() : element
      )
    ];
    List<Widget> elementsAr = [
      Expanded(
          flex: element == null ? 1 : 41,
          child: element == null ? Container() : element
      ),
      Expanded(
          flex: element == null ? 81 : 41,
          child: Text(text,style: style,
          textDirection: TextDirection.rtl,
          )
      ),
      Expanded(
          flex: 18,
          child: icon
      ),
    ];

    return Row(
        children: Lang.isAr ?elementsAr :elementsEn,

    );
  }
}

class MyButton extends StatelessWidget{
  final double fontFactor;
  final double sizeFactor;
  final String text;
  final GestureTapCallback onTap;
  final double heightFactor;

  AppTheme _appTheme = AppTheme();

  MyButton({this.text, this.onTap,this.fontFactor = 1,this.sizeFactor = 1,this.heightFactor = 1});

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return GestureDetector(

      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: h*0.145*sizeFactor*heightFactor
        ),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: h*.035),
          width: w*.4*sizeFactor,
          child: Center(


                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: w*.4*.25/fontFactor),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      text,
                      style: TextStyle(color: _appTheme.theme.buttonColor,
                      ),
                    ),
                  ),
                ),

          ),

          decoration: BoxDecoration(
              color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(h*.025*heightFactor*sizeFactor)),
            border: Border.all(
              color: _appTheme.theme.buttonColor,
            width: 1
            )
          ),
        ),
      ),
      onTap: onTap,
    );
  }
}

class MyTextField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final String hintText;
  final bool isPassword;
  final TextEditingController controller;

  MyTextField({this.onChanged, this.hintText,this.isPassword = false,this.controller});

  @override
  Widget build(BuildContext context) {
  double h = MediaQuery.of(context).size.height;
  double w = MediaQuery.of(context).size.width;
    return Container(
    margin: EdgeInsets.symmetric(horizontal:  w*.05),


      child: Stack(
        alignment: Alignment.topCenter,
        children: [
            Container(
                height: h*0.04,
                margin: EdgeInsets.only(top:h*0.0175),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Theme.of(context).accentColor
                  ),
                ),
              ),


          Directionality(
              textDirection: TextDirection.ltr,
              child: TextField(
                controller: controller,
            obscureText: isPassword,
          onChanged: onChanged,
            decoration: InputDecoration(
              hintText: hintText,
              fillColor: Color(0x00000000),
              filled: true,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,

            ),
            style: TextStyle(
              fontSize: h*0.02,
            ), textAlign:Lang.Align(),textDirection:TextDirection.ltr,


          )),
        ],
      ),
    );
  }
}

class MyTextArea extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final String titleText;
  final TextEditingController controller;

  MyTextArea({this.onChanged, this.titleText,this.controller});

  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(

      margin: EdgeInsets.symmetric(horizontal:  w*.05),
      child: Column(

        children: [
          Row(
            mainAxisAlignment: Lang.isAr ? MainAxisAlignment.end : MainAxisAlignment.start,
            children: [
              Text(titleText,
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: h*.025,
              ),
                textDirection:Lang.isAr ? TextDirection.rtl : TextDirection.ltr,
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).accentColor ),
            ),
            child: TextField(
              keyboardType: TextInputType.multiline,
              maxLines: 4,
              textAlign:Lang.Align(),textDirection:TextDirection.ltr,
              controller: controller,
            ),
          ),
        ],
      ),

    );
  }
}

class AnimatedLanguage extends StatefulWidget{
  final bool isE;
  final ValueChanged<bool> onChanged ;

  AnimatedLanguage({this.isE = true,this.onChanged});

  @override
  _AnimatedLanguageState createState() => _AnimatedLanguageState(isE,onChanged);
}

class _AnimatedLanguageState extends State<AnimatedLanguage> {
  bool _isE ;
  bool get isE => _isE;
  double _l , _r ;
  final ValueChanged<bool> onChanged ;



  _AnimatedLanguageState(this._isE,this.onChanged){
    if(isE){
      _l = 0;
      _r = 1;
    }
    else{
      _l = 1;
      _r = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    double inW = w*.4;

    return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(1),
        width: w*.8,
        height: h*.08,

        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Theme.of(context).buttonColor,
            width: 2
          ),
          borderRadius: BorderRadius.all(Radius.circular(h*.025)),

        ),
        child:Stack(
          children: [

            AnimatedContainer(
              duration: Duration(milliseconds: 150),
              width: w*.385,
              height: h*0.079,
              decoration: BoxDecoration(
                color: Theme.of(context).buttonColor,
                borderRadius: BorderRadius.all(Radius.circular(h*.025)),

              ),
              margin: EdgeInsets.only(left:_l*inW,right: _r*inW ),

            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("English",
                  style: TextStyle(
                    fontSize: h*.035,
                    color: isE?Colors.white:Theme.of(context).buttonColor,
                  ),
                  ),
                  Text("العربية",
                    style: TextStyle(
                      fontSize: h*.035,
                      color: !isE?Colors.white:Theme.of(context).buttonColor,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),


      ),
      onTap: (){
        setState(() {
          _isE= !_isE;
          if(_r>_l){
            _r = 0;
            _l = 1;
          }
          else{
            _l = 0;
            _r = 1;
          }
        });
        onChanged(_isE);
      },
    );
  }
}


class MyDropList extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final String hintText;

  MyDropList({this.onChanged, this.hintText});
  @override
  _MyDropListState createState() => _MyDropListState(hintText: hintText,onChanged: onChanged);
}

class _MyDropListState extends State<MyDropList> {
  final ValueChanged<String> onChanged;
  final String hintText;
  String _val;


  _MyDropListState({this.onChanged, this.hintText});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    TextStyle style = TextStyle(
      fontSize: h*.018,
      color: Theme.of(context).accentColor
    );
    return Container(
      margin: EdgeInsets.symmetric(horizontal:  w*.05,vertical: h*.0125),
          child:Container(
            height: h*0.04,
            margin: EdgeInsets.only(bottom: h*0.01,top: h*0.01),
            decoration: BoxDecoration(
              border: Border.all(
                  color: Theme.of(context).accentColor
              ),
            ),

            child: DropdownButton(
              hint: _val == null
                  ?Text('${Lang.get('SelectTransferMethod')} :',style: style, textAlign:Lang.Align(),textDirection:TextDirection.ltr,)
                  :Text(_val,style: style, textAlign:Lang.Align(),textDirection:TextDirection.ltr,),
              isExpanded: true,
              icon: Icon(Icons.keyboard_arrow_down),
              underline: Container(),
              items: [
                getItem(' ${Lang.get('Shipping')}',style ),
                getItem(' ${Lang.get('AirTransport')}',style ),
                getItem(' ${Lang.get('RoadTransport')}',style ),
                getItem(' ${Lang.get('Other')}',style )
              ],
              onChanged: (val){
                setState(() {
                  _val = val;
                  if(onChanged != null)
                    onChanged(val);
                });
              },

            ),

          ),

    );

  }
  DropdownMenuItem getItem(String text ,TextStyle style){
    return DropdownMenuItem(
      value: text,

      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:style.color) )
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(CupertinoIcons.circle_fill,size: style.fontSize*.6,color: style.color,),
                Text(text,style: TextStyle(
                  color: style.color,
                  fontSize: style.fontSize,

                ),),
              ],
            ),

          ],
        ),
      ),
    );

  }

}

class LinksRow extends StatelessWidget {
  final double factor;

  LinksRow({this.factor = 1});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height*0.07*factor,
      padding: EdgeInsets.symmetric(vertical:size.height*0.005),
      child: Directionality(
        textDirection: AppLanguage().isAr? TextDirection.rtl:TextDirection.ltr,
        child: LayoutBuilder(
          builder: (context,constrains) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                GestureDetector(
                  child: Container(
                    child: Image.asset('images/web${!AppTheme().isLight()}.png'),
                    margin: EdgeInsets.symmetric(horizontal: constrains.minWidth*0.03),
                  ),
                  onTap: ()async{
                    await launch('https://www.almajdscc.com/',
                      forceWebView: true,
                    );
                  },
                ),
                GestureDetector(
                  child: Container(
                    child: Image.asset('images/facebook.png'),
                    margin: EdgeInsets.symmetric(horizontal: constrains.minWidth*0.03),
                  ),
                  onTap: ()async{
                    await launch('https://m.facebook.com/almajdscc',
                      forceWebView: true,
                    );
                  },
                ),
                GestureDetector(
                  child: Container(
                    child: Image.asset('images/Instagram.png'),
                    margin: EdgeInsets.symmetric(horizontal: constrains.minWidth*0.03),
                  ),
                  onTap: ()async{
                    await launch('https://www.instagram.com/almajdscc/',
                      forceWebView: true,
                    );
                  },
                ),
                GestureDetector(
                  child: Container(
                    child: Image.asset('images/Linkedin.png'),
                    margin: EdgeInsets.symmetric(horizontal: constrains.minWidth*0.03),
                  ),
                  onTap: ()async{
                    await launch('https://www.linkedin.com/company/al-majd-shipping-&-customs-clearance',
                      forceWebView: true,
                    );
                  },
                ),



              ],
            );
          }
        ),
      ),
    );
  }
}

