import 'package:almajd/Language.dart';
import 'package:almajd/Screens/Elements/Elements.dart';
import 'package:almajd/Themes.dart';
import 'package:almajd/main.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import '../NavBarScreen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

AppLanguage Lang = AppLanguage();

//
// class OrderPost extends StatelessWidget {
//   order _Order = order.formJson({
//     "number":"22",
//     "type":"Computers",
//     "condition":"in sea",
//     "place":"America",
//     "wight":"120",
//     "quantity":"40",
//     "size":"12",
//     "code":"khaled-5",
//   });
//   State<Tracking> _Tracking;
//
//   OrderPost(this._Tracking,this._Order);
//   AppTheme _appTheme = AppTheme();
//
//   @override
//   Widget build(BuildContext context) {
//     double w = MediaQuery.of(context).size.width;
//     double h = MediaQuery.of(context).size.height;
//
//     TextStyle leftStyle = TextStyle(
//     color: _appTheme.theme.buttonColor,
//     fontSize: h*.02
//     );
//     TextStyle rightStyle = TextStyle(
//     color: _appTheme.theme.buttonColor,
//     fontSize: h*.02
//     );
//
//     TextStyle typeStyle = TextStyle(
//         color: _appTheme.theme.buttonColor,
//         fontSize: h*.018,
//       fontWeight: FontWeight.bold
//     );
//
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: w*.08,vertical: h*.01),
//       padding: EdgeInsets.symmetric(horizontal: w*.04,vertical: h*.005),
//       height: h*.2,
//       width: w*.85,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(w*.05),
//         color: _appTheme.isLight()?Color(0xFFEBEBEB):Color(0xFFFFFFFF),
//       ),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//
//               Container(
//                 height: h*.15,
//                 width: w*.37,
//                 color: Colors.transparent,
//                 padding: EdgeInsets.all(w*.37*.05),
//                 child: Column(
//                   crossAxisAlignment: Lang.isAr? CrossAxisAlignment.end:CrossAxisAlignment.start,
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text('${Lang.get("OrderNumber")} : ${_Order.number}',style: leftStyle,textAlign:Lang.Align(),textDirection:TextDirection.ltr,),
//                     Text('${Lang.get("OrderType")}',style: leftStyle,textAlign:Lang.Align(),textDirection:TextDirection.ltr,),
//                     Center(child: Text('${_Order.type}',style: typeStyle,textAlign:Lang.Align(),textDirection:TextDirection.ltr,))
//                   ],
//
//                 ),
//               ),
//               Container(
//                 width: w*.002,
//                 height: h*.2*.75,
//                 color: _appTheme.theme.buttonColor,
//                 margin: EdgeInsets.symmetric(vertical: h*.001),
//               ),
//               Container(
//                 height: h*.15,
//                 width: w*.37,
//                 padding: EdgeInsets.all(w*.37*.05),
//                 color: Colors.transparent,
//                 child: Column(
//                   crossAxisAlignment: Lang.isAr? CrossAxisAlignment.end:CrossAxisAlignment.start,
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//
//                     Text('${Lang.get("OrderCondition")} : ${_Order.condition}',style: rightStyle,textAlign:Lang.Align(),textDirection:Lang.Direction(),),
//                     Text('${Lang.get("OrderPlace")} : ${_Order.place}',style: rightStyle,textAlign:Lang.Align(),textDirection:Lang.Direction(),),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//           GestureDetector(
//             child: Container(
//               width: w*.37*.8,
//               padding: EdgeInsets.all(w*.0035),
//               margin: EdgeInsets.symmetric(vertical: h*.001),
//               decoration: BoxDecoration(
//                   color: Color(0xFFFFFFFF),
//                   borderRadius: BorderRadius.circular(w*.1),
//                 border: Border.all(color: _appTheme.theme.buttonColor)
//               ),
//               child: Center(
//                 child: Text(Lang.get('ViewMoreDetails'),style: TextStyle(
//                     color: _appTheme.theme.buttonColor,
//                     fontSize: h*.015
//                 ),),
//               ),
//             ),
//             onTap: (){
//               _Tracking.setState(() {
//                 popUp = PopUp(_Tracking,_Order);
//                 print('pop up button $popUp');
//               });
//             },
//           )
//
//         ],
//       ),
//     );
//   }
//
//
//
// }

class OrderList extends StatefulWidget {
  State<Tracking> _Tracking;

  OrderList(this._Tracking);

  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  bool _isFailed = false;

  Future<List<order>> _getDataAPI() async {
    var headers = {'Authorization': 'Bearer $token'};
    var request = http.Request(
        'GET', Uri.parse('https://almajd.almajdscc.com/api/auth/index'));

    request.headers.addAll(headers);

    http.StreamedResponse response = await request
        .send()
        .timeout(const Duration(seconds: 15), onTimeout: () {
      return null;
    });
    if (response == null) {
      _isFailed = true;
      return null;
    }

    if (response.statusCode == 200) {
      String JsonString = await response.stream.bytesToString();
      Map JsonData = json.decode(JsonString);

      if (JsonData["data"] == null) return [];
      List<order> orderList = [];
      for (var x in JsonData["data"]["customer"]["orders"])
        orderList.add(order.formJson({
          "number": x["Order_Number"],
          "type": x["fieldsMeta"][0]["Meta_Value"],
          "condition": x["Order_Status"],
          "place": x["statuses"]["Order_Location"],
          "wight": x["fieldsMeta"][3]["Meta_Value"],
          "quantity": x["fieldsMeta"][2]["Meta_Value"],
          "size": x["fieldsMeta"][4]["Meta_Value"],
          "code": x["fieldsMeta"][1]["Meta_Value"],
        }));
      orderList.sort((o1,o2){
        if( o1.condition == 'تم التسليم' && o2.condition != 'تم التسليم')
          return 1;
        if( o2.condition == 'تم التسليم' && o1.condition != 'تم التسليم')
          return -1;
        return int.parse(o2.number) -int.parse(o1.number);
      });
      return orderList;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<OrderPost> orders = [];

    return FutureBuilder(
        future: _getDataAPI(),
        builder: (BuildContext context, AsyncSnapshot<List<order>> snapShot) {
          if (snapShot.data == null)
            return Center(
                child: _isFailed
                    ? ErrorInLoad(size)
                    : CircularProgressIndicator());
          if (snapShot.data.isEmpty)
            return Center(
              child: Text('No Orders'),
            );
          List<OrderPost> orders = [];
          for (order o in snapShot.data)
            orders.add(OrderPost(widget._Tracking, o));
          return SingleChildScrollView(
            child: Column(
              children: orders,
            ),
          );
        });
  }

  Widget ErrorInLoad(Size size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          Lang.isAr ? 'خطأ في تحميل الشحنات' : 'Error In Loading The Items',
          style: TextStyle(
              color: AppTheme().theme.accentColor, fontSize: size.width * .05),
        ),
        GestureDetector(
          child: Container(
              padding: EdgeInsets.all(size.width * .02),
              child: Text(
                Lang.isAr ? 'اعادة المحاولة' : 'TRY AGAIN',
                style: TextStyle(
                    color: AppTheme().theme.accentColor,
                    fontSize: size.width * .035),
              ),
              color: Color(0xFF8F8F8F)),
          onTap: () {
            setState(() {
              _isFailed = false;
            });
          },
        )
      ],
    );
  }
}

class PopUp extends StatelessWidget {
  State<Tracking> _Tracking;
  AppTheme _appTheme = AppTheme();
  final order _Order;

  PopUp(this._Tracking, this._Order);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    List<Widget> dots = [];
    for (int i = 0; i < 28; i++)
      dots.add(Container(
        margin: EdgeInsets.symmetric(horizontal: w * .003),
        child: Icon(
          Icons.circle,
          color: _appTheme.theme.accentColor,
          size: h * .0075,
        ),
      ));

    TextStyle titleStyle =
        TextStyle(color: _appTheme.theme.accentColor, fontSize: 16);

    TextStyle valueStyle = TextStyle(
        color: _appTheme.theme.accentColor,
        fontSize: 14,
        fontWeight: FontWeight.bold);



    return GestureDetector(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color.fromRGBO(50, 50, 50, .5),
        child: GestureDetector(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: h * .16),
            padding: EdgeInsets.symmetric(horizontal: w * .1,vertical: w*0.075),
            width: double.infinity,

            decoration: BoxDecoration(
              color: _appTheme.boxDecoration.color,
              borderRadius: BorderRadius.circular(w * .075),
            ),
            child: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: w*0.055),
                  child: Row(
                    textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.close,
                          color: _appTheme.theme.accentColor,
                        ),
                        onPressed: () {
                          _Tracking.setState(() {
                            popUp = null;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        Lang.get("OrderDetails"),
                        style: TextStyle(
                            fontSize: w * .05,
                            color: _appTheme.theme.accentColor),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: dots,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TitleValue("${Lang.get("OrderNumber")}: ",
                              "${_Order.number}", titleStyle, valueStyle),
                          TitleValue("${Lang.get("OrderType")}: ",
                              "${_Order.type}", titleStyle, valueStyle),
                          TitleValue("${Lang.get("OrderCondition")}: ",
                              "${_Order.condition}", titleStyle, valueStyle),
                          TitleValue("${Lang.get("OrderPlace")}: ",
                              "${_Order.place}", titleStyle, valueStyle),
                          Container(
                            width: w * .95,
                            height: w * .005,
                            color: _appTheme.theme.accentColor,
                          ),
                          Container(
                            width: double.infinity,
                            child: Directionality(
                              textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TitleValue(
                                      "${Lang.get("OrderQuantity")}: ",
                                      "${_Order.quantity} Ctn",
                                      titleStyle,
                                      valueStyle),
                                  SizedBox(width: w * .1),
                                  TitleValue(
                                      "${Lang.get("OrderWeight")}: ",
                                      "${_Order.wight} Kgs",
                                      titleStyle,
                                      valueStyle),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            child: Directionality(
                              textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TitleValue("${Lang.get("OrderCode")}: ",
                                      "${_Order.code}", titleStyle, valueStyle),
                                  SizedBox(width: w * .1),
                                  TitleValue(
                                      "${Lang.get("OrderVolume")}: ",
                                      "${_Order.size} cbm",
                                      titleStyle,
                                      valueStyle),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: w * .95,
                            height: w * .005,
                            color: _appTheme.theme.accentColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          onTap: () {},
        ),
      ),
      onTap: () {
        _Tracking.setState(() {
          popUp = null;
        });
      },
    );
  }

  Widget TitleValue(
      String title, String value, TextStyle titleStyle, TextStyle valueStyle) {
    return Directionality(
      textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
      child: Row(
        //mainAxisAlignment: Lang.isAr ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [


           Text(title,
                  style: titleStyle,
                  textAlign: Lang.Align(),
                  textDirection: Lang.Direction())
          ,Text(value,
                  style: valueStyle,
                  textAlign: Lang.Align(),
                  textDirection: Lang.Direction()),
        ],
      ),
    );
  }

  Widget getAdd(String value, TextStyle titleStyle, TextStyle valueStyle) {
    String title = Lang.get("AdditionalInformation");
    return Row(
      mainAxisAlignment:
          Lang.isAr ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Lang.isAr
            ? Expanded(
                child: Text(
                value,
                style: valueStyle,
                textAlign: Lang.Align(),
                textDirection: Lang.Direction(),
              ))
            : Text('$title : ',
                style: titleStyle,
                textAlign: Lang.Align(),
                textDirection: Lang.Direction()),
        Lang.isAr
            ? Text('$title : ',
                style: titleStyle,
                textAlign: Lang.Align(),
                textDirection: Lang.Direction())
            : Expanded(
                child: Text(
                value,
                style: valueStyle,
                textAlign: Lang.Align(),
                textDirection: Lang.Direction(),
              )),
      ],
    );
  }
}

class order {
  final String number;
  final String type;
  final String condition;
  final String place;
  final String wight;
  final String quantity;
  final String size;
  final String code;

  order._p(this.number, this.type, this.condition, this.place, this.wight,
      this.quantity, this.size, this.code);

  static order formJson(Map M) {
    return order._p(
      M["number"],
      M["type"],
      M["condition"],
      M["place"],
      M["wight"],
      M["quantity"],
      M["size"],
      M["code"],
    );
  }

  @override
  String toString() {
    print("""Order Number    : $number
             Order Type      : $type
             Order Condition : $condition
             Order Place     : $place
             Order Wight     : $wight
             Order quantity  : $quantity
             Order Size      : $size
             Order Code      : $code
        """);
  }
}

class OrderPost extends StatelessWidget {
  State<Tracking> _Tracking;
  order _Order;

  OrderPost(this._Tracking, this._Order);

  @override
  Widget build(BuildContext context) {
    TextStyle leftStyle = TextStyle(
      color: AppTheme().theme.buttonColor,
    );
    TextStyle rightStyle = TextStyle(
      color: AppTheme().theme.buttonColor,
    );

    TextStyle typeStyle = TextStyle(
        color: AppTheme().theme.buttonColor, fontWeight: FontWeight.bold);

    Size size = MediaQuery.of(context).size;
    return Container(
        decoration: BoxDecoration(
          color: AppTheme().isLight() ? Color(0xFFEBEBEB) : Color(0xFFFFFFFF),
          borderRadius: BorderRadius.circular(size.width * 0.05),
        ),
        width: size.width * .9,
        height: size.height * .17,
        margin: EdgeInsets.symmetric(
            vertical: size.height * .01, horizontal: size.width * .05),
        child: LayoutBuilder(builder: (context, constrains) {
          return Column(
            children: [
              Row(
                textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: constrains.minWidth * .45,
                    child: Column(
                      crossAxisAlignment: Lang.isAr
                          ? CrossAxisAlignment.end
                          : CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                          children: [
                            FittedBox(
                                child: Text(
                              '${Lang.get("OrderNumber")}: ',
                              style: leftStyle,
                              textAlign: Lang.Align(),
                                  textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                            )),
                            FittedBox(
                                child: Text(
                                  '${_Order.number}',
                                  style: leftStyle.copyWith(fontWeight: FontWeight.bold),
                                  textAlign: Lang.Align(),
                                  textDirection: TextDirection.ltr,
                                )),
                          ],
                        ),
                        FittedBox(
                            child: Text(
                          '${Lang.get("OrderType")}:',
                          style: leftStyle,
                          textAlign: Lang.Align(),
                          textDirection: Lang.isAr?  TextDirection.rtl : TextDirection.ltr,
                        )),
                        Center(
                            child: FittedBox(
                                child: Text(
                          '${_Order.type}',
                          style: typeStyle,
                          textAlign: Lang.Align(),
                          textDirection: TextDirection.ltr,
                        )))
                      ],
                    ),
                  ),
                  Container(
                    width: 1,
                    height: constrains.maxHeight * .7,
                    color: AppTheme().theme.buttonColor,
                    margin: EdgeInsets.symmetric(
                        vertical: constrains.maxHeight * .04),
                  ),
                  Container(
                    width: constrains.minWidth * .45,
                    child: Column(
                      crossAxisAlignment: Lang.isAr
                          ? CrossAxisAlignment.end
                          : CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        FittedBox(
                            child: Row(
                              textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                              children: [
                                Text(
                                  '${Lang.get("OrderPlace")}: ',
                                  style: rightStyle,
                                  textAlign: Lang.Align(),
                                  textDirection: Lang.Direction(),
                                ),
                                Text('${_Order.place}',
                                  style: rightStyle.copyWith(fontWeight: FontWeight.bold),
                                  textAlign: Lang.Align(),
                                  textDirection: Lang.Direction(),
                                )
                              ],
                            )),
                        Column(
                          children: [
                            Text(
                          '${Lang.get("OrderCondition")}: ',
                          style: rightStyle,
                          textAlign: Lang.Align(),
                              textDirection: Lang.isAr ?  TextDirection.rtl: TextDirection.ltr,
                        ),
                            FittedBox(
                              alignment: Alignment.center,
                              child: Text('${_Order.condition}',
                                style: rightStyle.copyWith(fontWeight: FontWeight.bold),
                                textAlign: Lang.Align(),
                                textDirection: Lang.Direction(),

                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              GestureDetector(
                child: Container(
                  width: constrains.maxWidth * .45,
                  height: constrains.maxHeight * 0.18,
                  padding: EdgeInsets.symmetric(
                      horizontal: constrains.maxWidth * .02),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(constrains.minHeight * .1),
                      border: Border.all(color: AppTheme().theme.buttonColor),
                      color: Color(0xFFFFFFFF)),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      ' ${Lang.get('ViewMoreDetails')} ${AppLanguage().isAr ? ' ' : ''}',
                      style: TextStyle(
                          color: AppTheme().theme.buttonColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                onTap: () {
                  _Tracking.setState(() {
                    var size = MediaQuery.of(context).size;
                    popUp = PopUp(_Tracking, _Order);
                  });
                },
              ),
            ],
          );
        }));
  }
}
