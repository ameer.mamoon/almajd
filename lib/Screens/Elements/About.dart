import 'package:almajd/Language.dart';
import 'package:almajd/Themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'Elements.dart';

AppLanguage Lang = AppLanguage();

class AboutUs extends StatelessWidget {
  AppTheme _appTheme = AppTheme();

  @override
  Widget build(BuildContext context) {
    String text;
    if (Lang.isAr)
      text = """
المجد للشحن والتخليص الجمركي
      
نشأت شركة المجد للشحن والتخليص الجمركي عام 2000 كإحدى شركات الشحن الدولي و التخليص الجمركي و التصدير. ومنذ ذلك الحين و الشركة تعمل على تقديم أفضل واسرع خدمات الشخن و التخليص الجمركي في آن معاً.

سواء في مجال الشحن الجوي أو البحري أو البري تملك الشركة فريق عمل يمتلك من الدرة و المهارة و العملية التي تؤهله لتقديم افضل الخدمات في أسرع وقت ممكن.

اكتسبت الشركة السمعة التي تؤهلها للتنافي مع اكبر الشركات التي تشاركها في نفس المجال, و حازت على شرف التعامل مع اكبر الشركات في مجال التصدير و الاستيراد, لذى تسعى الشركة لتقديم افضل خدماتها و كذلك توسع قاعدة عملائها بما يتماشى مع متطلبات السوق.

ما تقدمه شركة المجد للتخليص الجمركي ليس مجرد عملية شحن , بل هو عمل متكامل من الخبرة و الاتقان و الاحتراف و سرعة التنفيذ , انه يسير قدماَ مع التطور الدائم و مواكبة التكنلوجيا المعاصرة مما يتيح للعميل الاستفادة من حلول ملائمة للشحن تناسب و توافق ادق شروطه و متطلباته.

و لتوفير أقصى ما يمكن للزبائن من خدمات و تسهيلات تميزت شركة المجد بالحرص الاكيد على سلامة البضائع المشحونة و العناية بها أثناء التداول و تأمين معلومات مستمرة عنها خلال عملية الشخن بالاضافة للإختصار الامثل للوقت بكافة مراحل الشحن , و التعاون و الاتفاق مع الزبون للتوصل لأفضل برنامج توريد او تصدير كامل لبضاعته و الالتزام به.

إن شروطكم و متطلباتكم أصبحت قواعد عمل لنا تواكب تعاوننا ضمن خدمات الشحن الدولي و الترانزيت مهما كانت معقدة و تفتح كل يوم آفاق لحلول مبتكرة.

نحن نضع بين ايدكم شبكة عالمية و محلية من وكلاء الشحن المحترفين تغطي كل بقاع العالم , تعمل بتناغم مطلق لتصل بضائعكم إلى مقصدها بسلامة و ضمن شروطكم مهما كانت الضروف.

السرعة الامان , الخدمة و الاسعار المنافسة... هي رمز العمل المتطور المتكامل و هي ما تقدمه لكم شركة المجد حول العالم.

أهداف الشركة :

1 - تقديم الخدمة من بدء استلام الشحنة حتى تسليميها في الوقت المحدد.

2 - إرضاء الزبون من خلال تقديم حلول شحن تحقق له الخدمات القيّمة المضافة.

3 - تحقيق عملية الشحن المثالية المستوفية لكافة متطلبات المستثمر من حيث التكلفة الأقل و الخدمة الأسرع متخذة من الخدمة الأفضل و السعر الأنسب شعاراَ لها.
""";
    else
      text = """
Al-Majd Shipping and Customs Clearance Company was established in 2000 as one of the international shipping, customs clearance and export companies. Since then, the company has been working to provide the best and fastest shipping and customs clearance services at the same time.

Whether in the field of air, sea or land freight, the company has a team that possesses the ability and scientific and practical skill that qualifies it to provide the best services as quickly as possible,

The company has gained the reputation that qualifies it to compete with the largest companies that share it in the same field, and has won the honor of dealing with the largest companies in the field of export and import, so the company seeks to provide the best services as well as expand its customer base in line with the requirements of the market.

What Al-Majd Shipping and Customs Clearance offers is not just a shipping process, it is an integrated work of experience, mastery, professionalism and speed of implementation. It is moving forward with constant development and keeping pace with contemporary technology, allowing the customer to benefit from convenient shipping solutions that fit and comply with the most accurate terms and requirements.

In order to provide the customers with the utmost services and facilities, Al-Majd Company was distinguished by its utmost concern for the safety of shipped goods and taking care of them during circulation and securing continuous information about them during the shipping process in addition to the optimal shortening of time at all stages of shipping, and cooperation and agreement with the customer to reach the best program for the supply or export of his goods and commitment to it .

Your terms and requirements have become our business rules that keep pace with our cooperation within the international freight and transit services, no matter how complicated they are, and that opens every day horizons for innovative solutions.We put in your hands a global and local network of professional shipping agents covering all parts of the world, working in absolute harmony to reach your goods to their destination safely and within your conditions, whatever the circumstances.

Speed, safety, service and competitive prices ... it is the symbol of an advanced, integrated work that is what Al-Majd Company offers you around the world.

The goals of the company:
1- Providing the service from the start of receiving the shipment until its delivery and on time.

2- Satisfying the customer by providing shipping solutions that provide him with value-added services.

3- Achieving the ideal shipping process that meets all the investor’s requirements in terms of the lowest cost and the fastest service, with the best service and the most appropriate price as its slogan.
              """;
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Directionality(
      textDirection: AppLanguage().isAr ? TextDirection.rtl : TextDirection.ltr,
      child: Stack(

        children: [
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(w * .025),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(w * .025),
                    child: Text(
                      Lang.get('AboutUs'),
                      style: TextStyle(
                        fontSize: h * .028,
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    alignment: Alignment.center,
                  ),
                  Container(
                    margin: EdgeInsets.all(w * .005),
                    child: Text(
                      text,
                      style: TextStyle(
                        fontSize: 15,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: h*0.075,
                  )
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                  child: LinksRow(factor: 0.7,),
                decoration: AppTheme().boxDecoration,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
