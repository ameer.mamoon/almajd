import 'package:almajd/Language.dart';
import 'package:almajd/Screens/Elements/Elements.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:almajd/main.dart';

import '../../Themes.dart';

AppLanguage Lang = AppLanguage();

class MainDrawer extends StatefulWidget {
  final bool isLogin;
  final int selector;
  MainDrawer(this.selector,{this.isLogin = false});

  @override
  _MainDrawerState createState() => _MainDrawerState(App);
}

class _MainDrawerState extends State<MainDrawer> {
  double h, w;

  BuildContext context;
  State _MyApp;
  AppTheme _appTheme = AppTheme();
  bool _LangSwitchValue = Lang.isAr;
  bool _ThemeSwitchValue;

  double _fontSize;

  _MainDrawerState(this._MyApp);

  @override
  void initState() {
    _ThemeSwitchValue = !_appTheme.isLight();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    _fontSize = h * .03;

    TextStyle style =
        TextStyle(fontSize: h * .025, color: Theme.of(context).accentColor);
    this.context = context;

    List<Widget> _items = [
      ListTile(),
      getUser(),
      getListTile(
          IconText(
            style: style,
            text: Lang.get('TrackShipment'),
            icon: Icon(CupertinoIcons.location),
          ), onTab: () {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, 'Tracking');
      },selected: 0 == widget.selector),
      getListTile(
          IconText(
              icon: Icon(Icons.info_outline),
              text: Lang.get('OurServices'),
              style: style), onTab: () {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, 'OurService');
      },
          selected: 4 == widget.selector),
      getListTile(
          IconText(
            icon: Icon(CupertinoIcons.chat_bubble_fill),
            text: Lang.get('AboutUs'),
            style: style,
          ), onTab: () {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, 'AboutUs');
      },
          selected: 1 == widget.selector
      ),
      getListTile(
          IconText(
            icon: Icon(Icons.phone),
            text: Lang.get('ContactUs'),
            style: style,
          ), onTab: () {
        Navigator.pop(context);
        Navigator.pushNamed(context, 'ContactUs');
      }
      ,selected: 2 == widget.selector
      ),
      getListTile(
          IconText(
            icon: Icon(Icons.settings),
            text: Lang.get('Settings'),
            style: style,
          ), onTab: () {
        Navigator.pop(context);
        Navigator.pushNamed(context, 'Settings');
      }),
    ];
    if (isLogin)
      _items.addAll([
        getListTile(
            IconText(
              icon: Icon(Icons.power_settings_new),
              text: Lang.get('SignOut'),
              style: style,
            ), onTab: () async {
          await showDialog(
              context: context,
              builder: (context) {
                Size size = MediaQuery.of(context).size;
                return Center(
                  child: Container(
                    width: size.width * 0.7,
                    height: size.height * 0.15,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      border: Border.all(color: Theme.of(context).accentColor),
                    ),
                    child: Column(
                      children: [
                        Expanded(
                            child: FittedBox(
                          child: Text(
                            'Logout?',
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.all(size.width * 0.01),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: FittedBox(
                                      child: GestureDetector(
                                    child: Text(Lang.isAr ? 'نعم' : 'YES',
                                        style: TextStyle(
                                          color: Theme.of(context).accentColor,
                                        )),
                                    onTap: () async {
                                      if (isLogin == false) return;

                                      await (await SavedData.get()).del('Name');
                                      await (await SavedData.get())
                                          .del('password');
                                      await (await SavedData.get())
                                          .del('token');

                                      await (await SavedData.get())
                                          .saveBool('isLogin', false);
                                      isLogin = false;
                                      Name = null;
                                      password = null;
                                      token = null;

                                      Navigator.pop(context);
                                      Navigator.popAndPushNamed(
                                          context, 'Tracking');
                                    },
                                  )),
                                ),
                                Expanded(
                                  child: FittedBox(
                                      child: GestureDetector(
                                    child: Text(Lang.isAr ? 'إلغاء' : 'CANCEL',
                                        style: TextStyle(
                                          color: Theme.of(context).accentColor,
                                        )),
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                  )),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              });
        }),

      ]);

    _items.add(SizedBox(height: h*0.15,));
    _items.add(LinksRow(factor: 0.7,));
    return Container(
      child: ListView(
        padding: EdgeInsets.zero,
        children: _items,
      ),
      decoration: _appTheme.boxDecoration,
    );
  }

  Widget getUser() {
    if (isLogin)
      {
        List<Widget> en = [
          Icon(
            Icons.person,
            color: Theme.of(context).buttonColor,
          ),

          Container(
            width: 1,
            height: double.infinity,
            color: Colors.black,
            margin: EdgeInsets.symmetric(horizontal: w*0.01),
          ),
          Text(
            ' $Name',
            style: TextStyle(
                color: Theme.of(context).buttonColor, fontSize: h * .02),
          )
        ];

        List<Widget> ar = [

          Text(
            ' $Name',
            style: TextStyle(
                color: Theme.of(context).buttonColor, fontSize: h * .02),
          ),
          Container(
            width: 1,
            height: double.infinity,
            color: Colors.black,
            margin: EdgeInsets.symmetric(horizontal: w*0.02),
          ),
          Icon(
            Icons.person,
            color: Theme.of(context).buttonColor,
          ),
        ];
        return getListTile(
            Container(
              margin: EdgeInsets.symmetric(horizontal: h*0.01,vertical: h*0.06).copyWith(bottom:h*0.02 ),
              height: h*0.06,
              child: Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: w*0.02),
              child: Row(
                mainAxisAlignment: Lang.isAr ?MainAxisAlignment.end:MainAxisAlignment.start,
                children: Lang.isAr ?ar:en
              ),
        ),
      ),first: true);
      }
    return Container();
  }

  Widget getListTile(Widget title, {onTab,selected = false,first = false}) {
    return GestureDetector(
      child: Container(
        color: selected? Colors.grey.withOpacity(0.4):Colors.transparent,
        margin:first?EdgeInsets.zero:  EdgeInsets.symmetric(vertical: h*0.0125).copyWith(left: w*0.05,right: w*0.1),
        child: title,

      ),
      onTap: onTab,
    );
  }

  Widget Line() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: w * 0.035),
      color: Theme.of(context).accentColor,
      height: h * .0015,
    );
  }
}
