import 'package:almajd/Language.dart';
import 'package:almajd/Screens/Elements/About.dart';
import 'package:almajd/Screens/MainPage.dart';
import 'package:almajd/Screens/Welcome.dart';
import 'package:almajd/Screens/inputs.dart';
import 'package:almajd/Themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'Screens/NavBarScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

State App;
String Name,password;
String token ;
bool isLogin = false;

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppTheme _appTheme;
  AppLanguage Lang ;
  SavedData _savedData;

  void getSaved() async{
    _savedData = (await SavedData.get());
  }

  @override
  void initState() {
    getSaved();

  }

  @override
  Widget build(BuildContext context){

    print('build !!!!!!!!!!!!');
    _appTheme = AppTheme();
    _appTheme.correct();
    Lang = AppLanguage();

    App = this;
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);


        return MaterialApp(
          theme: _appTheme.theme,

          initialRoute: 'Welcome',
          routes: {
            'Welcome':(context) => Welcome(),
            'Settings':(context) => Settings(this),
            'Login':(context) =>NavScreen(Login(),Nlogin: false,selector: 0,),
            'Language':(context) => MainPage(child: Language(),),
            'ChangePass':(context) => MainPage(child: ChangePass(),),
            'ContactUs':(context) => NavScreen( ContactUs(),selector: 2,),
            'AboutUs':(context) => NavScreen(AboutUs(),selector: 1,),
            'Tracking':(context) => Tracking(),
            'OurService':(context) => NavScreen(OurService(),selector: 4,),
          },
          builder: (BuildContext context,Widget child){
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                child: child);
          },

        );

  }


}

class SavedData {

  static SavedData _Instance = SavedData._C();
  Map<String,dynamic> data = {
    'firstOpen':false,
    'isLight':true,
    'isAr':false,
    'isLogin':false,
    'token':null,
    'Name':null,
    'Password':null
  };

  SavedData._C(){

  }

  static Future<SavedData> get() async{
   await _Instance.getData();
    return _Instance;
  }

  Future<void> getData() async{
    var pref = await SharedPreferences.getInstance();
    if(pref.getBool('firstOpen') == null){
      print('first Open');

      await pref.setBool('firstOpen', data['firstOpen']);
      await pref.setBool('isLight', data['isLight']);
      await pref.setBool('isAr', data['isAr']);
      await pref.setBool('isLogin', data['isLogin']);
      //await pref.setString('token', data['token']);
      //await pref.setString('Name', data['Name']);
      //await pref.setString('Password', data['Password']);
      data['firstOpen'] = true;
      return;
    }
    print('not first***********************************');

    bool isLight =  pref.getBool('isLight');
    data['isLight'] = isLight ?? true;

    bool isAr =  pref.getBool('isAr');
    data['isAr'] = isAr ?? false;

    bool firstOpen =  false;
    data['firstOpen'] = firstOpen;

    bool IsLogin = pref.getBool('isLogin');
    data['isLogin'] = IsLogin ?? false;

    String Token = pref.getString('token');
    data['token'] =IsLogin? Token:null ;

    String name = pref.getString('Name');
    data['Name'] = name ;

    String Password = pref.getString('Password');
    data['Password'] = Password ;

    AppTheme.setInit(data['isLight']);
    AppLanguage.setInit(data['isAr']);
    isLogin = data['isLogin'];
    token = data['token'];
    Name = data['Name'];
    password = data['Password'];
  }

  dynamic getValue(String key){
    return data[key];
  }

  Future<void> saveBool(String key,bool value) async {
    print('val : $value');
    var pref = await SharedPreferences.getInstance();
    await pref.setBool(key, value);
  }

  Future<void> saveString(String key,String value) async {
    await del(key);
    var pref = await SharedPreferences.getInstance();
    print('Prefff:$pref, key $key, value $value');
    await pref.setString(key, value);
  }

  Future<void> del(String key)async{
    var pref = await SharedPreferences.getInstance();
    await pref.remove(key);
  }
}

