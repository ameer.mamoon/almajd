import 'package:almajd/main.dart';
import 'package:flutter/material.dart';

class AppTheme {

  static AppTheme _Instance= null;
  static bool _isLight = true;
  void getSaved() async {_savedData = await SavedData.get();}
  SavedData _savedData ;
  AppTheme._Const(){
    getSaved();
    if(_isLight){
    _theme = _Light;
    _boxDecoration = _boxDecorationLight;
    _ThemeIcon = _LightIcon;
    }
    else{
      _theme = _Dark;
      _boxDecoration = _boxDecorationDark;
      _ThemeIcon = _DarkIcon;
    }

  }



  ThemeData _Light = ThemeData(
    appBarTheme: AppBarTheme(
      backgroundColor: Color(0x00FFFFFF),
      elevation: 0,
    ),
    scaffoldBackgroundColor: Colors.white,
      brightness: Brightness.light,
    iconTheme: IconThemeData(
      color: Color.fromRGBO(18, 51, 122, 1),
    ),
    accentColor: Color.fromRGBO(18, 51, 122, 1),
    buttonColor: Color.fromRGBO(18, 51, 122, 1),
    fontFamily: 'Cairo',
    bottomAppBarColor: Colors.white,
    primaryColor: Colors.white,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(18, 51, 122, 1),
      ),
    ),

  );

  ThemeData _Dark = ThemeData(
    appBarTheme: AppBarTheme(
      backgroundColor: Color(0xFF010101),
      elevation: 0,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: Colors.white
      ),
    ),

    scaffoldBackgroundColor: Colors.black,
      brightness: Brightness.dark,
      iconTheme: IconThemeData(
          color: Colors.white
      ),
      accentColor: Colors.white,
      buttonColor: Colors.black,
    fontFamily: 'Cairo',
    bottomAppBarColor: Color(0xFFF6F6F6),
  );

  ThemeData _theme ;

  BoxDecoration _boxDecorationLight = BoxDecoration(
    gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [Color(0xFFFFFFFF),Color(0xFFF2F2F0)]
    ),
    color: Color(0xFFF2F2F0)
  );

  BoxDecoration _boxDecorationDark = BoxDecoration(
    color:Color(0xFF171717)
  );

  BoxDecoration _boxDecoration ;

  Icon _DarkIcon = Icon(Icons.nightlight_round,color: Colors.white,);

  Icon _LightIcon = Icon(Icons.wb_sunny_outlined,color: Color(0xFF385773),);

  Icon _ThemeIcon;

  factory AppTheme(){
    if(_Instance == null){
      _Instance = AppTheme._Const();
    }
    return _Instance;
  }

  void SwitchTheme(){
    _isLight = !_isLight;
    //if light --> dark
    if(_theme == _Light && _boxDecoration == _boxDecorationLight){
      _theme = _Dark;
      _boxDecoration = _boxDecorationDark;
      _ThemeIcon = _DarkIcon;
    }
    //if dark --> light
    else{
    _theme = _Light;
    _boxDecoration = _boxDecorationLight;
    _ThemeIcon = _LightIcon;
  }
    _savedData.saveBool('isLight', _isLight);
  }



  ThemeData get theme => _theme;

  BoxDecoration get boxDecoration => _boxDecoration;

  Icon  ThemeIcon(double size){

    return Icon(_ThemeIcon.icon,color: _ThemeIcon.color,size: size,);
  }

  bool isLight(){
    return _theme == _Light;
  }

  static void setInit(bool IsLight){
    print('Set init $IsLight');
    _isLight = IsLight;
  }
  void correct(){
    if(_isLight){
      _theme = _Light;
      _boxDecoration = _boxDecorationLight;
      _ThemeIcon = _LightIcon;
    }
    else{
      _theme = _Dark;
      _boxDecoration = _boxDecorationDark;
      _ThemeIcon = _DarkIcon;
    }
  }
}