
import 'dart:ui';

import 'package:almajd/main.dart';

class AppLanguage{

  static AppLanguage _Instance;
  static bool _isAr = false;
  AppLanguage._Const(){
     getSaved();
  }

  factory AppLanguage(){
    if( _Instance == null)
      _Instance = AppLanguage._Const();
    return _Instance;
  }

  Map<String,String> _En={
    "Settings":"Settings",
    "Routing":"Routing",
    "AboutUs":"About Us",
    "ContactUs":"Contact Us",
    "Services":"Services",
    "Tracking":"Tracking Shipment",
    "TrackShipment":"Track Shipment",
    "Login":"Log in",
    "SignOut":"Sign Out",
    "OurServices":"Our Services",
    "Shipping":"Shipping",
    "AirTransport":"Air Transport",
    "RoadTransport":"Road Transport",
    "OrderNumber":"Order number",
    "OrderType":"Order Type",
    "OrderCondition":"Order Condition",
    "OrderPlace":"Order Place",
    "Language":"Language",
    "Theme":"Theme",
    "Confirm":"Confirm",
    "Name":"Name",
    "PhoneNumber":"Phone Number",
    "Email":"Email",
    "SelectTransferMethod":"Select a transfer method",
    "DescriptionAboutTheOrder":"Message",
    "ViewMoreDetails":"view more details",
    "Enter":"Enter",
    "Password":"Password",
    "UserName":"User Name",
    "ChangePassword":"Change Password",
    "OldPassword":"Old Password",
    "NewPassword":"New Password",
    "ChooseYourLanguage":"Choose your language",
    "you can change this later in the":"you can change this later in the",
    "OrderDetails":"Order Details",
    "AdditionalInformation":"Additional\nInformation",
    "OrderQuantity":"Quantity",
    "OrderCode":"Order Code",
    "OrderWeight":"Weight",
    "OrderVolume":"Volume",
    "Latest":"latest",
    "ForgotPassword":"Forgot Password ?",
    "Other":"Other",
    "Send":"SEND"
  };

  Map<String,String> _Ar={
    "Settings":"الاعدادات",
    "Routing":"تتبع",
    "AboutUs":"من نحن",
    "ContactUs":"تواصل معنا",
    "Services":"الخدمات",
    "Tracking":"تتبع الشحنات",
    "TrackShipment":"تتبع الشحنات",
    "Login":"تسجيل الدخول",
    "SignOut":"تسجيل الخروج",
    "OurServices":"خدماتنا",
    "Shipping":"بحري",
    "AirTransport":"جوي",
    "RoadTransport":"بريّ",
    "OrderNumber":"رقم الشحنة",
    "OrderType":"نوع البضاعة",
    "OrderCondition":"حالة الشحنة",
    "OrderPlace":"مصدر الشحنة",
    "Language":"اللغة",
    "Theme":"المظهر",
    "Confirm":"تأكيد",
    "Name":"الاسم",
    "PhoneNumber":"رقم الجوال",
    "Email":"البريد الالكتروني",
    "SelectTransferMethod":"اختر طريقة الشحن",
    "DescriptionAboutTheOrder":"الرسالة",
    "ViewMoreDetails":"عرض تفاصيل الشحنة",
    "Enter":"ادخل",
    "Password":"كلمة السر",
    "UserName":"اسم المستخدم",
    "ChangePassword":"تغيير كلمة السر",
    "OldPassword":"كلمة السر القديمة",
    "NewPassword":"كلمة السر الجديدة",
    "ChooseYourLanguage":"اختر لغة التطبيق",
    "you can change this later in the":"يمكنك تغيرها لاحقاَ في",
    "OrderDetails":"تفاصيل الطلب",
    "AdditionalInformation":"معلومات\nإضافية",
    "OrderQuantity":"عدد الطرود",
    "OrderCode":"رمز الشحنة",
    "OrderWeight":"الوزن",
    "OrderVolume":"الحجم",
    "Latest":"الاحدث",
    "ForgotPassword":"نسيت كلمة المرور ؟",
    "Other":"غير ذلك",
    "Send":"إرسال"
  };

  void getSaved() async {_savedData = await SavedData.get();}
  SavedData _savedData ;

  Future<void> SwitchLanguage() async{
    _isAr = !_isAr;

    _savedData.saveBool('isAr', _isAr);
  }

  String get(String key){
    return _isAr?_Ar[key]:_En[key];
  }

  bool get isAr => _isAr;

  TextAlign Align(){
    return _isAr?TextAlign.end:TextAlign.start;
  }

  TextDirection Direction(){
    print('is Ar ? : $_isAr');
    return _isAr?TextDirection.rtl:TextDirection.ltr;
  }

  static void setInit(bool init){
    print('SetInit lang $init');
    _isAr = init;
  }

  void correct(){

  }

}